// bad implementation, use an enum?
function getNextHeaderClass(this_header_class)
{
    if (this_header_class == "ltoc_h1")
    {
        return "ltoc_h2";
    }
    else if (this_header_class == "ltoc_h2")
    {
        return "ltoc_h3";
    }
    else if (this_header_class == "ltoc_h3")
    {
        return "ltoc_h4";
    }
    else if (this_header_class == "ltoc_h4")
    {
        return "ltoc_h5";
    }
    else if (this_header_class == "ltoc_h5")
    {
        return "ltoc_h6";
    }
    else if (this_header_class == "ltoc_h6")
    {
        return "ltoc_h7";
    }
    return null;
}

// bad implementation, use an enum?
function headerClassGreaterThan(a, b)
{
    if (a == "ltoc_h1")
    {
        return (b == "ltoc_h2" || b == "ltoc_h3" || b == "ltoc_h4" || b == "ltoc_h5" || b == "ltoc_h6" || b == "ltoc_h7");
    }
    else if (a == "ltoc_h2")
    {
        return (b == "ltoc_h3" || b == "ltoc_h4" || b == "ltoc_h5" || b == "ltoc_h6" || b == "ltoc_h7");
    }
    else if (a == "ltoc_h3")
    {
        return (b == "ltoc_h4" || b == "ltoc_h5" || b == "ltoc_h6" || b == "ltoc_h7");
    }
    else if (a == "ltoc_h4")
    {
        return (b == "ltoc_h5" || b == "ltoc_h6" || b == "ltoc_h7");
    }
    else if (a == "ltoc_h5")
    {
        return (b == "ltoc_h6" || b == "ltoc_h7");
    }
    else if (a == "ltoc_h6")
    {
        return (b == "ltoc_h7");
    }
    return false;
}

function addOnClickFunction(coll)
{
    let i = 0;
    for (i = 0; i < coll.length; i++)
    {
        coll[i].addEventListener("click", function()
        {
            this.classList.toggle("sb_active");
            this_header_class = this.classList[0];
            next_header_class = getNextHeaderClass(this_header_class);
            if (next_header_class == null)
            {
                return;
            }
            let j = 0;
            let content = this.nextElementSibling;
            while (content)
            {
                content_header_class = content.classList[0];
                if (content_header_class == this_header_class)
                {
                    break;
                }
                else if (content.style.display === "none")
                {
                    // open content one only tag more than the one we clicked on
                    if (content_header_class == next_header_class)
                    {
                        content.style.display = "inline-block";
                    }
                }
                else if (content.style.display === "inline-block")
                {
                    // close everything the tag name we clicked on is greater than
                    if (headerClassGreaterThan(this_header_class, content_header_class))
                    {
                        content.style.display = "none";
                    }
                }
                else
                {
                    // weird edge case for first load which should be fixed elsewhere
                    // then this code removed
                    if (content_header_class == next_header_class)
                    {
                        content.style.display = "inline-block";
                    }
                }
                content = content.nextElementSibling;
                j = j + 1;
            }
        });
    }
}

let coll = document.getElementsByClassName("ltoc_h1");
addOnClickFunction(coll);
coll = document.getElementsByClassName("ltoc_h2");
addOnClickFunction(coll);
coll = document.getElementsByClassName("ltoc_h3");
addOnClickFunction(coll);
coll = document.getElementsByClassName("ltoc_h4");
addOnClickFunction(coll);
coll = document.getElementsByClassName("ltoc_h5");
addOnClickFunction(coll);
coll = document.getElementsByClassName("ltoc_h6");
addOnClickFunction(coll);
